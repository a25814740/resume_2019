var $html = $('html');

// 套件
var frameworkFunc = {
    AOS: function () {
        setTimeout(() => {
            AOS.init();
        }, 1500);
    }
}

// 功能
var toolsFunc = {
    scroll_top: function () {
        $('.scrollTop').click(function(){
            $("html, body").animate({ 
                scrollTop: 0 
            }, 1000, 'easeInOutQuint');
            return false;
        });
    },
    scroll_to_id: function () {
        $('.scrollToID').click(function(){
            var target = $(this).attr('data-target');
            $("html, body").animate({ 
                scrollTop: $(target).offset().top
            }, 1000, 'easeInOutQuint');
            return false;
        });
    }
}

// 偵測PC、手機瀏覽器
var browserDetect = {
    ios: function() {
        var ios = /iPhone|iPad|iPod/i.test(navigator.userAgent);
        if (!ios) {
            $html.removeClass("ios-mode");
        } else {
            $html.addClass("ios-mode");
            
            var w = $('.wp').width(),
                h = window.screen.height;
            $('.wp > .bg').css('width', w);
            $('.wp > .bg').css('height', h);
        }
    },
    ie: function() {
        var IE = /MSIE|Trident/i.test(window.navigator.userAgent);
        if (!IE) {
            $html.removeClass("IE-mode");
        } else {
            $html.addClass("IE-mode");
        }
    }
}

// 首頁
var indexFunc = {
    windowLoad: function () {
        $('.wp').addClass('page-in');
    }
}

var readyFunction = {
    checkFunction: function () {

        var functionName = $('.wp').attr('data-page');

        if (functionName !== undefined) {
            eval("readyFunction." + functionName + "();");
        }
        
    },
    index: function () {
        toolsFunc.scroll_top()
        toolsFunc.scroll_to_id()
        browserDetect.ios()
    }
}

$(document).ready(function () {
    readyFunction.checkFunction()
})

$(window).on('load', function (){
    indexFunc.windowLoad()
    frameworkFunc.AOS()
});